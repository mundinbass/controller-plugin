﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    public float GravityModifier = 1;

    protected Rigidbody2D rb2d;
    protected Vector2 velocity;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected const float shellRadius = 0.01f;

    protected ContactFilter2D contactFilter;
    protected const float minMoveDistance = 0.001f;

    void Start()
    { 
    
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
    }
    void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        velocity += GravityModifier * Physics2D.gravity * Time.deltaTime;

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 move = Vector2.up * deltaPosition.y;

        Movement(move);
    }

    void Movement(Vector2 a_move)
    {
        float distance = a_move.magnitude;

        if (distance > minMoveDistance)
        {
            int count = rb2d.Cast(a_move, contactFilter, hitBuffer, distance + shellRadius);
        }

        rb2d.position = rb2d.position + a_move;
    }
}
