﻿using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

    public Texture2D map;

    public ColorToPrefabs[] colorMappings;


	// Use this for initialization
	void Start ()
    {
        GenerateLevel();
	}

    void GenerateLevel()
    {
        for (int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                GenerateTile(x, y);
            }
        }
    }

    void GenerateTile(int x, int y)
    {
        Color pixelColor = map.GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            // Empty, do nothing
            return;
        }

        Debug.Log(pixelColor);

        foreach (ColorToPrefabs colorMapping in colorMappings)
        {
            if (colorMapping.colour.Equals(pixelColor)) 
            {
                if (colorMapping.prefab == null)
                    continue;
                // if the colour in the map equals a colour in the colorMappings array, it matches
                Vector2 position = new Vector2(x, y);
                Instantiate(colorMapping.prefab, position, Quaternion.identity, transform);
                //Debug.Log("I went through");
            }
        }


    }
}
