﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public unsafe class ControllerInterface : MonoBehaviour
{
    [StructLayout(LayoutKind.Sequential)]
    public struct OutputHandler // struct exported and copied directly from plugin
    {
        public bool keya, keyb, keyx, keyy, dpadup, dpaddown, dpadright, dpadleft, lshoulder, rshoulder, keyback, keystart, akey; // akey for debug purposes
        public int ltrigger, rtrigger, thumblx, thumbly, thumbrx, thumbry;
    }

    [DllImport("ControllerPlugin")]
    public static extern int updateControllers(OutputHandler[] oh);
    [DllImport("ControllerPlugin")]
    public static extern void initSystem();
    [DllImport("ControllerPlugin")]
    public static extern void initializeControllers(OutputHandler[] oh, int size);
    [DllImport("ControllerPlugin")]
    public static extern void CleanUpSystem();
    [DllImport("ControllerPlugin")]
    public static extern bool returnTrue(); // always returns true; to check if dll is linking properly

    OutputHandler[] controller_array = new OutputHandler[1]; // master array
    
    int temp; // avoid dynamic allocation

    

    // Use this for initialization
    void Start()
    {
        initSystem();
        initializeControllers(controller_array, 1);
        //temp = updateControllers(controller_array);
        Debug.Log(returnTrue());
        //Debug.Log(temp);
    }

    // Update is called once per frame
    void Update()
    {
        temp = updateControllers(controller_array);
        if (controller_array[0].dpaddown)                   // using [0] because I've only initialized 1 controller. 
            Debug.Log("Dpad Down was clicked!");            // have not tried more than 1 controller yet
        if (controller_array[0].dpadup)
            Debug.Log("Dpad Up was clicked!");
        if (controller_array[0].dpadright)
            Debug.Log("Dpad Right was clicked!");
        if (controller_array[0].dpadleft)
            Debug.Log("Dpad Left was clicked!");
        if (controller_array[0].keya)
            Debug.Log("Button A was clicked!");
        if (controller_array[0].keyb)
            Debug.Log("Button B was clicked!");
        if (controller_array[0].keyx)
            Debug.Log("Button X was clicked!");
        if (controller_array[0].keyy)
            Debug.Log("Button Y was clicked!");
        Debug.Log(controller_array[0].akey); // keyboard doesnt work either
        Debug.Log(controller_array[0].lshoulder);
        Debug.Log(temp); // Was the controller found?

    }

}
