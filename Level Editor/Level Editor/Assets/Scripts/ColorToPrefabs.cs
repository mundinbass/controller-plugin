﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorToPrefabs
{
    public Color colour;
    public GameObject prefab;
}
