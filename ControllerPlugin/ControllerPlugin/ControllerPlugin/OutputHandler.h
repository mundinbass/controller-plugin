#pragma once
#include <windows.h>
#include <XInput.h>

// XInput Library
#pragma comment(lib, "XInput.lib")

extern "C"
{
	struct __declspec(dllexport) OutputHandler // exported.  "__declspec(dllexport) struct OutputHandler" returns a warning C4091 "no variable is declared" 
	{                                           // when "warnings treated as errors" is on, error C2220, "no 'object' file generated"
												// both still work fine. As it is, returns no warnings or errors
		bool keya, keyb, keyx, keyy, dpadup, dpaddown, dpadright, dpadleft, lshoulder, rshoulder, keyback, keystart, akey; // akey is for debug only
		int ltrigger, rtrigger, thumblx, thumbly, thumbrx, thumbry;
	};
}

class ControllerSystem
{
public:
	ControllerSystem();
	~ControllerSystem();
	int maxControllers;
	int update(OutputHandler controllers[]);
};
