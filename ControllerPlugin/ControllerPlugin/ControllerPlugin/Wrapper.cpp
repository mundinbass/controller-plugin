#include "Wrapper.h"
#include <iostream>
#include <iomanip>
#include <Windows.h> // for debug, used to get "A" key state

ControllerSystem *CS;

int updateControllers(OutputHandler *oh)
{
	int temp;
	temp = CS->update(oh);
	return temp;
}																			

void initSystem()
{
	CS = new ControllerSystem();
}

void initializeControllers(OutputHandler *oh, int size)
{
	CS->maxControllers = size;

	for (int i = 0; i < size; i++)
	{
		oh[i].dpaddown = false;
		oh[i].dpadup = false;
		oh[i].dpadright = false;
		oh[i].dpadleft = false;
		oh[i].keya = false;
		oh[i].keyb = false;
		oh[i].keyx = false;
		oh[i].keyy = false;
		oh[i].keyback = false;
		oh[i].keystart = false;
		oh[i].lshoulder = false;
		oh[i].rshoulder = false;
		oh[i].rtrigger = 0;
		oh[i].ltrigger = 0;
		oh[i].thumblx = 0;
		oh[i].thumbly = 0;
		oh[i].thumbrx = 0;
		oh[i].thumbry = 0;
	}
}

void CleanUpSystem()
{
	delete CS;
}

bool returnTrue()
{
	return true;
}


//int main()
//{
//	OutputHandler *cont = new OutputHandler();
//	initSystem();
//	initializeControllers(cont, 1);
//	bool willExit = false;
//	std::cout << std::boolalpha;
//
//	while (true)
//	{
//		updateControllers(cont);
//
//		std::cout << "Instructions:\n";
//			//std::cout << "[A] Vibrate Left Only\n";
//			//std::cout << "[B] Vibrate Right Only\n";
//			//std::cout << "[X] Vibrate Both\n";
//			//std::cout << "[Y] Vibrate Neither\n";
//			std::cout << "[BACK] or [START] Exit\n\n";
//
//			for (int i = 0; i < CS->maxControllers; i++)
//			{
//				if (cont[i].keystart || cont[i].keyback)
//				{
//					willExit = true;
//					break;
//				}
//
//				std::cout << "CONTROLLER " << i + 1 << "---" << std::endl;
//				// Output state of buttons
//
//				std::cout << "BUTTON A: " << (cont + i)->keya << std::endl;
//				std::cout << "BUTTON B: " << cont[i].keyb << std::endl;
//				std::cout << "BUTTON X: " << cont[i].keyx << std::endl;
//				std::cout << "BUTTON Y: " << cont[i].keyy << std::endl << std::endl;
//
//				// Output state of Dpad buttons
//
//				std::cout << "DPAD - UP:  " << cont[i].dpadup << std::endl;
//				std::cout << "DPAD - LEFT:  " << cont[i].dpadleft << std::endl;
//				std::cout << "DPAD - RIGHT:  " << cont[i].dpadright << std::endl;
//				std::cout << "DPAD - DOWN:  " << cont[i].dpaddown << std::endl << std::endl;
//
//				// Output state of shoulders
//				std::cout << "LEFT SHOULDER: " << cont[i].lshoulder << std::endl;
//				std::cout << "RIGHT SHOULDER: " << cont[i].rshoulder << std::endl << std::endl;
//
//
//
//				// Output the trigger values
//
//				std::cout << "\rLeft Trigger (0-255): " << std::setw(3) << std::setfill(' ') << cont[i].ltrigger << std::endl;
//				std::cout << "\rRight Trigger (0-255): " << std::setw(3) << std::setfill(' ') << cont[i].rtrigger << std::endl << std::endl;
//
//				// Output Thumbstick values
//				std::cout << "\rLeft Thumbstick X-value (0 +/- 32768): " << std::setw(5) << std::setfill(' ') << cont[i].thumblx << std::endl;
//				std::cout << "\rLeft Thumbstick Y-value (0 +/- 32768): " << std::setw(5) << std::setfill(' ') << cont[i].thumbly << std::endl;
//				std::cout << "\rRight Thumbstick X-value (0 +/- 32768): " << std::setw(5) << std::setfill(' ') << cont[i].thumbrx << std::endl;
//				std::cout << "\rRight Thumbstick Y-value (0 +/- 32768): " << std::setw(5) << std::setfill(' ') << cont[i].thumbry << std::endl << std::endl;
//
//				std::cout << "A keybaord  " << (GetKeyState('A') & 0x8000) << std::endl << std::endl; // debug
//			}
//		
//			system("cls");
//			if (willExit)
//				break;
//	}
//	CleanUpSystem();
//	return 0;
//}