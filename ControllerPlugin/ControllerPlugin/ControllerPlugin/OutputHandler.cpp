#include "OutputHandler.h"
#include <iostream>
#include <fstream>

enum errorcode // for debug use
{
	CONTROLLER_NOT_FOUND,
	SUCCESS
};

ControllerSystem::ControllerSystem()
{

}

ControllerSystem::~ControllerSystem()
{

}
//int temp;  // debug
int ControllerSystem::update(OutputHandler controllers[]) // int return type is debug, to check if controller was found at runtime
{
	
	DWORD dwResult;
	for (int i = 0; i < maxControllers; i++)
	{
		//temp++;  // debug, check if this is happening often
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		// Simply get the state of the controller from XInput.
		dwResult = XInputGetState(i, &state);

		// If controller is not connected
		if (!dwResult == ERROR_SUCCESS)
		{
			//return CONTROLLER_NOT_FOUND; // debug return
			//std::cout << "error!!"; // debug
			continue;
		}
		//std::cout << "success" << dwResult << temp << std::endl << i;  // debug
		// Update all controllers
		controllers[i].keya = (state.Gamepad.wButtons & XINPUT_GAMEPAD_A);
		controllers[i].keyb = (state.Gamepad.wButtons & XINPUT_GAMEPAD_B);
		controllers[i].keyx = (state.Gamepad.wButtons & XINPUT_GAMEPAD_X);
		controllers[i].keyy = (state.Gamepad.wButtons & XINPUT_GAMEPAD_Y);
		//std::cout << (state.Gamepad.wButtons & XINPUT_GAMEPAD_A);   // debug
		//std::cout << controllers[i].keya; // debug
		controllers[i].dpaddown = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN);
		controllers[i].dpadup = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP);
		controllers[i].dpadright = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT);
		controllers[i].dpadleft = (state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT);

		controllers[i].lshoulder = (state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER);
		controllers[i].rshoulder = (state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER);

		controllers[i].ltrigger = (int)state.Gamepad.bLeftTrigger;
		controllers[i].rtrigger = (int)state.Gamepad.bRightTrigger;

		controllers[i].thumblx = (int)state.Gamepad.sThumbLX;
		controllers[i].thumbly = (int)state.Gamepad.sThumbLY;
		controllers[i].thumbrx = (int)state.Gamepad.sThumbRX;
		controllers[i].thumbry = (int)state.Gamepad.sThumbRY;

		controllers[i].keyback = (state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK);
		controllers[i].keystart = (state.Gamepad.wButtons & XINPUT_GAMEPAD_START);

		controllers[i].akey = (GetKeyState('A') & 0x8000);
	}
	return SUCCESS; // debug return
}