#pragma once
#include "OutputHandler.h"

extern "C"
{
	//__declspec(dllexport) void initSystem();
	//__declspec(dllexport) void CleanUpSystem();
	//__declspec(dllexport) void initializeParticles(Particle *p, int size, int MaxDistance);
	//__declspec(dllexport) void updateSystemPosition(float x, float y, float z);
	//__declspec(dllexport) void updateAttraction(float f);
	//__declspec(dllexport) void updateParticles(Particle *p, float dt);

	__declspec(dllexport) void initSystem();
	__declspec(dllexport) void initializeControllers(OutputHandler *oh, int size);
	__declspec(dllexport) int updateControllers(OutputHandler *oh);
	__declspec(dllexport) void CleanUpSystem();
	__declspec(dllexport) bool returnTrue();
}
